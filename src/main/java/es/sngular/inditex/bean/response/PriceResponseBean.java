package es.sngular.inditex.bean.response;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PriceResponseBean {

  private Long productId;

  private Long brandId;

  private Long priceList;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  private Double price;

  private String curr;
}
