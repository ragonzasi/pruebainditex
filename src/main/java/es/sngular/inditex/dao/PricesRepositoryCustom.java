package es.sngular.inditex.dao;

import java.time.LocalDateTime;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.sngular.inditex.entity.PriceEntity;

@Repository
public interface PricesRepositoryCustom {

  public PriceEntity getPriceInfo(
      @Param("brandId") Long brandId,
      @Param("productId") Long productId,
      @Param("date") LocalDateTime date);
}
