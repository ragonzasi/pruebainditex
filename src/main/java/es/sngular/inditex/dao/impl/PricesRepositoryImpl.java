package es.sngular.inditex.dao.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.sngular.inditex.dao.PricesRepositoryCustom;
import es.sngular.inditex.entity.PriceEntity;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class PricesRepositoryImpl implements PricesRepositoryCustom {

  private final EntityManager em;

  @Override
  public PriceEntity getPriceInfo(Long brandId, Long productId, LocalDateTime date) {

    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<PriceEntity> cq = cb.createQuery(PriceEntity.class);

    Root<PriceEntity> price = cq.from(PriceEntity.class);
    Predicate brandPred = cb.equal(price.get("brandId"), brandId);
    Predicate productPred = cb.equal(price.get("productId"), productId);
    Predicate startDatePred = cb.lessThanOrEqualTo(price.get("startDate"), date);
    Predicate endDatePred = cb.greaterThanOrEqualTo(price.get("endDate"), date);

    cq.where(brandPred, productPred, startDatePred, endDatePred);
    cq.orderBy(cb.desc(price.get("prioirity")));

    TypedQuery<PriceEntity> query = em.createQuery(cq);
    query.setMaxResults(1);

    List<PriceEntity> result = query.getResultList();
    if (result.isEmpty()) {
      return null;
    }
    return result.get(0);
  }
}
