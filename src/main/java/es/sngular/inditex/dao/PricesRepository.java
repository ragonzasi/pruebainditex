package es.sngular.inditex.dao;

import org.springframework.data.repository.CrudRepository;

import es.sngular.inditex.entity.PriceEntity;

public interface PricesRepository
    extends CrudRepository<PriceEntity, Long>, PricesRepositoryCustom {}
