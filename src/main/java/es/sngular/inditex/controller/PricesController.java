package es.sngular.inditex.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.sngular.inditex.bean.response.PriceResponseBean;
import es.sngular.inditex.mapper.PricesMapper;
import es.sngular.inditex.service.PricesService;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@RequestMapping("/price")
public class PricesController {

  private final PricesService service;

  @ApiOperation(value = "Get price Info", notes = "")
  @GetMapping("/{brandId}/{productId}/{date}")
  public ResponseEntity<PriceResponseBean> getPriceInfo(
      @NonNull @PathVariable("brandId") Long brandId,
      @NonNull @PathVariable("productId") Long productId,
      @NonNull @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
          LocalDateTime date) {

    return ResponseEntity.ok(PricesMapper.map(service.getPriceInfo(brandId, productId, date)));
  }
}
