package es.sngular.inditex.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Value("${swagger.enabled:false}")
  private boolean swaggerenabled;

  @Bean
  public Docket api() {

    return new Docket(DocumentationType.SWAGGER_2)
        .forCodeGeneration(true)
        .enable(swaggerenabled)
        .select()
        .apis(RequestHandlerSelectors.basePackage("es.sngular.inditex.controller"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo())
        .useDefaultResponseMessages(false);
  }

  private ApiInfo apiInfo() {

    return new ApiInfoBuilder()
        .title("Prueba Técnica")
        .description("Prueba Técnica Rafael González Simón")
        .version("0.0.1-SNAPSHOT")
        .contact(
            new Contact(
                "Rafael González Simón",
                "https://www.sngular.com/es/",
                "rafael.gonzalez@sngular.com"))
        .build();
  }
}
