package es.sngular.inditex.mapper;

import es.sngular.inditex.bean.PriceBean;
import es.sngular.inditex.bean.response.PriceResponseBean;
import es.sngular.inditex.entity.PriceEntity;
import lombok.experimental.UtilityClass;

@UtilityClass
public class PricesMapper {

  public static PriceBean map(PriceEntity entity) {
    return null != entity
        ? PriceBean.builder()
            .brandId(entity.getBrandId())
            .curr(entity.getCurr())
            .endDate(entity.getEndDate())
            .id(entity.getId())
            .price(entity.getPrice())
            .priceList(entity.getPriceList())
            .prioirity(entity.getPrioirity())
            .productId(entity.getProductId())
            .startDate(entity.getStartDate())
            .build()
        : null;
  }

  public static PriceResponseBean map(PriceBean bean) {
    return null != bean
        ? PriceResponseBean.builder()
            .brandId(bean.getBrandId())
            .curr(bean.getCurr())
            .endDate(bean.getEndDate())
            .price(bean.getPrice())
            .priceList(bean.getPriceList())
            .productId(bean.getProductId())
            .startDate(bean.getStartDate())
            .build()
        : null;
  }
}
