package es.sngular.inditex.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.sngular.inditex.bean.PriceBean;
import es.sngular.inditex.dao.PricesRepository;
import es.sngular.inditex.mapper.PricesMapper;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class PricesService {

  private final PricesRepository repository;

  public PriceBean getPriceInfo(Long brandId, Long productId, LocalDateTime date) {

    return PricesMapper.map(repository.getPriceInfo(brandId, productId, date));
  }
}
