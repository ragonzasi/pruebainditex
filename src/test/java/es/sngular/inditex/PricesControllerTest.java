package es.sngular.inditex;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import es.sngular.inditex.bean.response.PriceResponseBean;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PricesControllerTest {

  @LocalServerPort private int port;

  @Autowired private TestRestTemplate restTemplate;

  @Test
  public void priceTest() throws Exception {

    // {date for filter, wanted price result}
    String[][] testDates =
        new String[][] {
          {"2020-06-14 10:00:00", "35.50"},
          {"2020-06-14 16:00:00", "25.45"},
          {"2020-06-14 21:00:00", "35.50"},
          {"2020-06-15 10:00:00", "30.50"},
          {"2020-06-16 21:00:00", "38.95"}
        };

    PriceResponseBean result;
    for (String[] strings : testDates) {

      result =
          this.restTemplate.getForObject(
              "http://localhost:" + port + "/price/1/35455/" + strings[0], PriceResponseBean.class);

      assertThat(result).isNotNull();

      assertEquals(Double.valueOf(strings[1]), result.getPrice());
    }
  }

  @Test
  public void priceTestNoData() throws Exception {

    PriceResponseBean result =
        this.restTemplate.getForObject(
            "http://localhost:" + port + "/price/1/35455/2021-06-16 21:00:00",
            PriceResponseBean.class);

    assertThat(result).isNull();
  }
}
