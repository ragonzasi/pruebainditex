package es.sngular.inditex;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import es.sngular.inditex.controller.PricesController;

@SpringBootTest
class PricesApplicationTests {

  @Autowired PricesController controller;

  @Test
  void contextLoads() {

    assertThat(controller).isNotNull();
  }
}
